﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            ClassA A = new ClassA();
            ClassB B = new ClassB();            
            Console.WriteLine(A.AccessSinglenton());
            Console.WriteLine(B.AccessSinglenton());
            Console.ReadKey();
        }
    }
}
