﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    public class ClassA
    {        
        public string AccessSinglenton()
        {
            return string.Format("Accesing to singleton from Class A with Serial {0}", Singleton.GetInstance.SharedData);
        }
    }
}
